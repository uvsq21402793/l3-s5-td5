package fr.uvsq.Fraction.Fraction;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest {

	@Test
	public void testConstructeur() {
		App f = new App();
		assertEquals(0, f.getNumer());
		assertEquals(1, f.getDenom());
	}

	@Test
	public void testConstructeur2() {
		App f = new App(2, 2);
		assertEquals(2, f.getNumer());
		assertEquals(2, f.getDenom());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructeur2Fail() {
		App f = new App(2, 0);
		assertEquals(2, f.getNumer());
		assertEquals(2, f.getDenom());
	}

	@Test
	public void testConstructeur3() {
		App f = new App(2);
		assertEquals(2, f.getNumer());
		assertEquals(1, f.getDenom());
	}

	@Test
	public void testDouble() {
		App f = new App(3, 2);
		double i = f.getDouble();
	}

}
