package fr.uvsq.Fraction.Fraction;

public class App {
	private int numerateur;
	private int denominateur;

	public App() {
		this.numerateur = 0;
		this.denominateur = 1;
	}

	public App(int n, int d) throws IllegalArgumentException {
		this.numerateur = n;
		if (d > 0)
			this.denominateur = d;
		else
			throw new IllegalArgumentException();
	}

	public App(int n) {
		this.numerateur = n;
		this.denominateur = 1;
	}

	public int getNumer() {
		return this.numerateur;
	}

	public int getDenom() {
		return this.denominateur;
	}

	public double getDouble() {
		return numerateur / denominateur;
	}

}
